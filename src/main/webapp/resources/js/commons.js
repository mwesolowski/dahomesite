function addArticle() {
	var tytul = $('#tytulArtykulu').val();
	var wprowadzenie = $('#wprowadzenie').val();
	var tresc = $('#tresc').val();
	
	$.ajax({
		type: "POST",
		url: "/daHomeSite/articles/add",
		data: { tytulArtykulu: tytul, wprowadzenie: wprowadzenie, tresc: tresc },
		success: function(response) {
			$('#alert').html(response);
		},
		error: function(e) {

		}
	});
}

function editArticle() {
	var id = $('#id').val();
	var tytul = $('#tytulArtykulu').val();
	var wprowadzenie = $('#wprowadzenie').val();
	var tresc = $('#tresc').val();
	
	$.ajax({
		type: "POST",
		url: "/daHomeSite/articles/edit",
		data: { id: id, 
				tytulArtykulu: tytul, 
				wprowadzenie: wprowadzenie, 
				tresc: tresc
				},
		success: function(response) {
			$('#alert').html(response);
		},
		error: function(e) {

		}
	});
}
	
function countCharInField(val) {
	
	var limit = $(val).attr("maxlength");
	var fieldName = $(val).attr("id");
	var leftSpan = fieldName + "Pozostalo";
	
	
	$('#' + fieldName).keyup(function() {
		var len = $(this).val().length;
		
		$('#' + leftSpan).text(limit-len);
		
		console.log(len);
	});
}

function ajaxDestroyArticle(val) {
		var id_artykulu = $(val).attr("value");
        
        bootbox.dialog("Potwierdź usunięcie artykułu.", [{
                "label" : "TAK",
                "class" : "btn-success",
                "callback": function() {
                	$.ajax({
                		type: "DELETE",
                		url: "/daHomeSite/articles/delete/" + id_artykulu,
                		data: { id: id_artykulu
                				},
                		success: function(response) {
                			$("#alert").addClass('alert alert-success');
                			$("#alert").html(response);
                			
                			
                			// odswiez liste artykulow
                			$.ajax( {
                				url : "/daHomeSite/articles/ajax",
                				success: function(data) {
                					$("#table").html(data);
                				}
                			});
                			
                		},
                		error: function(e) {
                			$("#alert").addClass('alert alert-error');
                			$("#alert").html(e.responseText);
                		}
                	});
                   	
                }
            }, {
                "label" : "NIE",
                "class" : "btn-danger",
                "callback": function() {
              
                }
            }]);  
}

function ajaxDestroySubpage(val) {
	
		var id_podstrony = $(val).attr("value");
                   
        bootbox.dialog("Potwierdź usunięcie podstrony.", [{
                "label" : "TAK",
                "class" : "btn-success",
                "callback": function() {
                	$.ajax({
                		type: "DELETE",
                		url: "/daHomeSite/subpages/delete/" + id_podstrony,
                		data: { id: id_podstrony
                				},
                		success: function(response) {
                			$("#alert").addClass('alert alert-success');
                			$("#alert").html(response);
                			
                			// odswiez liste podstron
                			$.ajax( {
                				url : "/daHomeSite/subpages/ajax",
                				success: function(data) {
                					$("#table").html(data);
                				}
                			});
                		},
                		error: function(e) {
                			$("#alert").addClass('alert alert-error');
                			$("#alert").html(e.responseText);
                		}
                	});
                }
            }, {
                "label" : "NIE",
                "class" : "btn-danger",
                "callback": function() {
                }
            }]);

}

function ajaxDestroyCategory(val) {
	var id_kategorii = $(val).attr("value");
    
    bootbox.dialog("Potwierdź usunięcie kategorii.", [{
            "label" : "TAK",
            "class" : "btn-success",
            "callback": function() {
            	$.ajax({
            		type: "DELETE",
            		url: "/daHomeSite/categories/" + id_kategorii,
            		data: { id: id_kategorii
            				},
            		success: function(response) {
            			$("#alert").removeClass('alert alert-error').addClass('alert alert-success');
            			$("#alert").html(response);
            			
            			
            			// odswiez liste kategorii
            			$.ajax( {
            				url : "/daHomeSite/categories/ajax",
            				success: function(data) {
            					$("#table").html(data);
            				}
            			});
            		},
            		error: function(e) {
            			$("#alert").removeClass('alert alert-success').addClass('alert alert-error');
            			$("#alert").html(e.responseText);
            		}
            	});
      			
            }
        }, {
            "label" : "NIE",
            "class" : "btn-danger",
            "callback": function() {
            	
            }
        }]);
}
	