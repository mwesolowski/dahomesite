package pl.daHomeSite.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import pl.daHomeSite.entities.Subpage;

@Component
public interface SubpageMapper {
	
	final String INSERT = "INSERT INTO subpages(tytul,zawartosc) VALUES (#{tytul},#{zawartosc})";
	final String SELECT_ALL_FOR_CHOOSING = "SELECT id,tytul FROM subpages";
	final String SELECT_SINGLE_SUBPAGE = "SELECT * FROM subpages WHERE id=#{id}";
	final String UPDATE_SUBPAGE = "UPDATE subpages SET tytul=#{tytul},zawartosc=#{zawartosc} WHERE id=#{id}";
	final String DELETE = "DELETE FROM subpages WHERE id=#{id}";
	
	@Insert(INSERT)
	@Options(useGeneratedKeys = true, keyProperty="id")
	public void add(Subpage s);

	@Select(SELECT_ALL_FOR_CHOOSING)
	@Results(value = {
			@Result(property="id", column="id"),
			@Result(property="tytul", column="tytul")
	})
	public List<Subpage> getAllSubpages();

	@Select(SELECT_SINGLE_SUBPAGE)
	@Results (value = {
			@Result(property="id", column="id"),
			@Result(property="tytul", column="tytul"),
			@Result(property="zawartosc", column="zawartosc")
	})
	public Subpage getSubpage(int id);

	@Update(UPDATE_SUBPAGE)
	public void updateSubpage(Subpage s);

	@Delete(DELETE)
	public void delete(Subpage s);

}
