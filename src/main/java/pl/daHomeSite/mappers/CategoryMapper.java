package pl.daHomeSite.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import pl.daHomeSite.entities.Category;
import pl.daHomeSite.entities.Subpage;

@Repository
public interface CategoryMapper {
	
	final String INSERT = "INSERT INTO categories(nazwa) VALUES (#{nazwa})";
	final String SELECT_CATEGORY_BY_NAME = "SELECT id,nazwa FROM categories WHERE nazwa=#{nazwa}";
	final String SELECT_ALL_CATEGORIES = "SELECT id,nazwa FROM categories";
	final String SELECT_CATEGORY_BY_ID = "SELECT id,nazwa FROM categories WHERE id=#{id}";
	final String UPDATE_CATEGORY = "UPDATE categories SET nazwa=#{nazwa} WHERE id=#{id}";
	final String DELETE_CATEGORY = "DELETE FROM categories WHERE id=#{id}";
	final String COUNT_ARTICLES_IN_CATEGORY = "SELECT COUNT(*) FROM articles WHERE id_category=#{id}";
	
	
	@Insert(INSERT)
	@Options(useGeneratedKeys = true, keyProperty="id")
	public void add(Category c);

	@Select(SELECT_CATEGORY_BY_NAME)
	@Results (value = {
			@Result(property="id", column="id"),
			@Result(property="nazwa", column="nazwa")
	})
	public Category findCategoryByName(String name);

	@Select(SELECT_ALL_CATEGORIES)
	@Results (value = {
			@Result(property="id", column="id"),
			@Result(property="nazwa", column="nazwa")
	})
	public List<Category> getAllCategories();

	@Select(SELECT_CATEGORY_BY_ID)
	@Results (value = {
			@Result(property="id", column="id"),
			@Result(property="nazwa", column="nazwa")
	})
	public Category getCategoryById(int categoryId);

	@Update(UPDATE_CATEGORY)
	public void updateCategory(Category c);

	@Delete(DELETE_CATEGORY)
	public void deleteCategory(Category c);
	
	@Select(COUNT_ARTICLES_IN_CATEGORY)
	public int countArticlesInCategory(Category c);
}
