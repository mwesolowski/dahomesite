package pl.daHomeSite.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import pl.daHomeSite.entities.Article;
import pl.daHomeSite.entities.Category;

@Component
public interface ArticleMapper {
	
	final String INSERT = "INSERT INTO articles(tytul,wprowadzenie,tresc,data_artykulu,id_category) VALUES (#{tytulArtykulu},#{wprowadzenie},#{tresc},#{dataArtykulu},#{kategoria.id})";
	final String UPDATE = "UPDATE articles SET tytul=#{tytulArtykulu},wprowadzenie=#{wprowadzenie},tresc=#{tresc},id_category=#{kategoria.id} WHERE id=#{id}";
	final String DELETE = "DELETE FROM articles WHERE id=#{id}";
	final String SELECT_ALL = "SELECT * FROM articles a  "
			+ "LEFT JOIN categories c ON"
			+ " a.id_category=c.id";
	final String SELECT_ALL_FOR_CHOOSING = "SELECT a.id as article_id,tytul,id_category FROM articles a " +
			"LEFT JOIN categories c ON " + 
			"a.id_category=c.id";
	final String SELECT_ONE_ARTICLE = "SELECT a.id as article_id,tytul,wprowadzenie,tresc,data_artykulu,id_category "
			+ "FROM articles a "
			+ "LEFT JOIN categories c ON a.id_category=c.id "
			+ "WHERE a.id=#{id}";
	final String SELECT_ALL_WITHOUT_CONTENT = "SELECT a.id as article_id,tytul,wprowadzenie,data_artykulu,id_category FROM articles a "
		+ " LEFT JOIN categories c ON id_category=c.id"
		+ " ORDER BY data_artykulu DESC";
	
	@Insert(INSERT)
	@Options(useGeneratedKeys = true, keyProperty="id")
	public void add(Article a);
	
	@Update(UPDATE)
	public void update(Article a);
	
	@Delete(DELETE)
	public void delete(Article a);
	
	@Select(SELECT_ALL)
	@Results(value = {
			@Result(property="id", column="id"),
			@Result(property="tytulArtykulu", column="tytul"),
			@Result(property="wprowadzenie", column="wprowadzenie"),
			@Result(property="tresc",column="tresc"),
			@Result(property="kategoria", column="id_category", javaType=Category.class, 
				one=@One(select="pl.daHomeSite.mappers.CategoryMapper.getCategoryById"))
			
			
	})
	public List<Article> getAll();
	
	@Select(SELECT_ALL_FOR_CHOOSING)
	@Results(value = {
			@Result(property="id", column="article_id"),
			@Result(property="tytulArtykulu", column="tytul"),
			@Result(property="kategoria", column="id_category", javaType=Category.class, 
				one=@One(select="pl.daHomeSite.mappers.CategoryMapper.getCategoryById"))
	})
	public List<Article> getAllArticlesForChoosing();
	
	@Select(SELECT_ONE_ARTICLE)
	@Results(value = {
			@Result(property="id", column="article_id"),
			@Result(property="tytulArtykulu", column="tytul"),
			@Result(property="wprowadzenie",column="wprowadzenie"),
			@Result(property="tresc",column="tresc"),
			@Result(property="dataArtykulu", column="data_artykulu"),
			@Result(property="kategoria", column="id_category", javaType=Category.class, 
				one=@One(select="pl.daHomeSite.mappers.CategoryMapper.getCategoryById"))
	})
	public Article getSingleArticle(int idArtykulu);
	
	@Select(SELECT_ALL_WITHOUT_CONTENT)
	@Results(value = {
			@Result(property="id", column="id"),
			@Result(property="tytulArtykulu", column="tytul"),
			@Result(property="wprowadzenie", column="wprowadzenie"),
			@Result(property="dataArtykulu", column="data_artykulu"),
			@Result(property="kategoria", column="id_category", javaType=Category.class, 
				one=@One(select="pl.daHomeSite.mappers.CategoryMapper.getCategoryById"))
	})
	public List<Article> getAllWithoutContent();
}
