package pl.daHomeSite.validators;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import pl.daHomeSite.entities.Article;
import pl.daHomeSite.entities.Category;
import pl.daHomeSite.services.ArticleService;
import pl.daHomeSite.services.CategoryService;

@Component
public class ArticleValidator implements Validator{

	private static final Logger log = Logger.getLogger(ArticleValidator.class);

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return Article.class.equals(clazz);
	}

	@Override
	public void validate(Object o, Errors e) {
		
		log.debug("start");
		
		Article a = (Article) o;
		
		String tytulArtykulu = a.getTytulArtykulu().trim();
		String wprowadzenie = a.getWprowadzenie().trim();
		String tresc = a.getTresc().trim();
		Category kategoria = a.getKategoria();
		
		if (tytulArtykulu.length() == 0) {
			log.debug("tytul - puste pole");
			e.rejectValue("tytulArtykulu", "required", "Tytuł artykułu nie może być pusty.");
		}
		
		if (tytulArtykulu.length() > 150) {
			log.debug("tytul - przekracza 150 znakow");
			e.rejectValue("tytulArtykulu", "required", "Tytuł artykułu przekracza 150 znaków.");
		}
		
		if (wprowadzenie.length() == 0) {
			log.debug("wprowadzenie - puste pole");
			e.rejectValue("wprowadzenie", "required", "Wprowadzenie do artykułu nie może być pustym polem.");
		}
		
		if (wprowadzenie.length() > 255) {
			log.debug("wprowadzenie - przekracza 255 znakow");
			e.rejectValue("wprowadzenie", "required", "Tytuł artykułu przekracza 255 znaków.");
		}
		
		if (tresc.length() == 0) {
			log.debug("tresc - puste pole");
			e.rejectValue("tresc", "required", "Treść artykułu nie może być pusta.");
		}
		
		if (tresc.length() > 1000) {
			log.debug("tresc - przekracza 1000 znakow");
			e.rejectValue("tresc", "required", "Treść artykułu przekracza 1000 znaków.");
		}
		
		if (kategoria == null) {
			log.debug("kategoria - nie wybrano");
			e.rejectValue("kategoria",  "required", "Nie wybrano kategorii.");
		}
		
		
		log.debug("finish");
		
	}

}
