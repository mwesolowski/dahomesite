package pl.daHomeSite.validators;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import pl.daHomeSite.entities.Category;
import pl.daHomeSite.services.CategoryService;

@Component
public class CategoryValidator implements Validator{

	private static final Logger log = Logger.getLogger(CategoryValidator.class);

	@Autowired
	private CategoryService categoryService;
	
	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return Category.class.equals(clazz);
	}

	@Override
	public void validate(Object o, Errors e) {
		
		log.debug("start");
		
		Category c = (Category) o;
		String nazwa = c.getNazwa().trim();
		
		if (nazwa.length() == 0) {
			log.debug("nazwa pusta");
			e.rejectValue("nazwa", "required", "Nazwa kategorii nie może być pusta.");
		}
		
		if (nazwa.length() > 255) {
			log.debug("nazwa powyzej 255 znakow");
			e.rejectValue("nazwa", "required", "Nazwa kategorii przekracza 255 znaków");
		}
		
		if (categoryService.findCategoryByName(nazwa) != null) {
			log.debug("kategoria juz istnieje");
			e.rejectValue("nazwa",  "required", "Kategoria o podanej nazwie już istnieje w bazie danych.");
		}
		//Category x = categoryService.findCategoryByName("xxx");
		
		log.debug("finish");
		
	}

}
