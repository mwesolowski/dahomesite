package pl.daHomeSite.validators;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import pl.daHomeSite.entities.Subpage;

@Component
public class SubpageValidator implements Validator {

	private static final Logger log = Logger.getLogger(SubpageValidator.class);

	@Override
	public boolean supports(Class<?> clazz) {
	    // TODO Auto-generated method stub
	    return Subpage.class.equals(clazz);
	}

	@Override
	public void validate(Object o, Errors e) {
	    log.debug("start");
		
	    	Subpage s = (Subpage) o;
		String tytul = s.getTytul();
		String zawartosc = s.getZawartosc();
		
		if (tytul.trim().length() == 0) {
			log.debug("tytul - puste pole");
			e.rejectValue("tytul", "required", "Tytuł nie może być pusty.");
		}
		
		if (tytul.trim().length() > 255) {
			log.debug("tytul - przekracza 255 znakow");
			e.rejectValue("tytul", "required", "Tytuł przekracza 255 znaków.");
		}
		
		if (zawartosc.trim().length() == 0) {
			log.debug("zawartosc - puste pole");
			e.rejectValue("zawartosc", "required", "Zawartość podstrony nie może być pusta.");
		}

		log.debug("finish");
	    
	}

}
