package pl.daHomeSite.controllers;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/admin")
public class AdminPanel {
    
    private static final Logger log = Logger.getLogger(AdminPanel.class);

    @RequestMapping(method = RequestMethod.GET)
    public String indexAdminPanel() {
	log.debug("start");
	log.debug("finish");
	return "admin/index";
    }
}
