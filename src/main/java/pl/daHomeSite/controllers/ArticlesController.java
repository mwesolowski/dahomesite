package pl.daHomeSite.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pl.daHomeSite.entities.Article;
import pl.daHomeSite.services.ArticleService;
import pl.daHomeSite.services.CategoryService;
import pl.daHomeSite.utils.Sanitizer;
import pl.daHomeSite.validators.ArticleValidator;

@Controller
@RequestMapping("/articles")
public class ArticlesController {
	
	private static final Logger log = Logger.getLogger(ArticlesController.class);
	
	@Autowired
	private ArticleService articleService;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private ArticleValidator articleValidator;
	
	// Pokazuje formularz dodawania artykułu
	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public String newArticle(ModelMap model) {
		log.debug("start");
		
		/*klucz "action" w modelu steruje nazwą nagłówka jaki się pojawi na formularzu
		* jeśli wartość będzie "new" - pokaże się nagłówek "Dodaj artykuł"
		* wykorzystywany jest jeden widok do dodawania oraz aktualizacji treści artykułu
		*
		*/
		model.addAttribute("action","new");
		
		model.addAttribute("article", new Article());
		
		// w formularzu artykułu należy wybrać kategorię - jest lista pobierana z bazy danych, a następnie wyświetlana w select boxie
		model.addAttribute("categoryList",categoryService.getAllCategories());
		
		log.debug("finish");
		return "articles/addOrUpdateArticle"; 
	}
	
	// Wykonanie operacji dodania artykułu
	@RequestMapping(method = RequestMethod.POST)
	public String createArticle(@ModelAttribute(value="article")Article a, 
			BindingResult result,
			ModelMap model) {
	    
	    	// Wyświetlam do loggera informację o początku działania metody
		log.debug("start");
			
		/*klucz "action" w modelu steruje nazwą nagłówka jaki się pojawi na formularzu
		* jeśli wartość będzie "new" - pokaże się nagłówek "Dodaj artykuł"
		* wykorzystywany jest jeden widok do dodawania oraz aktualizacji treści artykułu
		*
		*/
		model.addAttribute("action","new");
			
		// inicjalizacja obiektu Category w klasie Article
		int categoryId = a.getKategoria().getId();
		a.setKategoria(categoryService.getCategoryById(categoryId));
		
		// walidator dla artykułu - sprawdza czy wszystkie pola są wypełnione
		articleValidator.validate(a, result);
		
		if (result.hasErrors()) {
			log.debug("validation errors");
			log.debug("finish");
			
			// w przypadku wystąpienia błędów podczas walidacji artykułu - walidator sprawdza, czy wszystkie pola są wypełnione, jeśli nie to zwraca błąd
			model.addAttribute("action","new");
			model.addAttribute("categoryList",categoryService.getAllCategories());
			return "articles/addOrUpdateArticle";
		}
		String articleContent = a.getTresc();
		
		// oczyszczam treść artykułu z niebezpiecznych tagów HTML np <script> utworzonych przez edytor TinyMCE
		// aby uniknąć podstawowych ataków typu XSS
		articleContent = Sanitizer.sanitizeHTML(articleContent);
		a.setTresc(articleContent);
			
		// metoda warstwy usługi służąca do dodania obiektu klasy Article do bazy danych
		articleService.add(a);
			
		/* Po dodaniu artykułu przygotowuję nowy obiekt do wyświetlenia w modelu, założyłem, że osoba która
		 * dodała artykuł będzie chciała od razu dodać kolejny. 
		 */
		model.addAttribute("article", new Article());
		model.addAttribute("categoryList",categoryService.getAllCategories());
			
		// jeśli wartość klucza "success" wynosi true, wyświetla się komunikat o pomyślnym dodaniu artykułu do bazy
		model.addAttribute("success",true);
							
		log.debug("finish");
		return "articles/addOrUpdateArticle";
			
	}
	
	// shows all articles and options to edit, delete, read
	@RequestMapping(method = RequestMethod.GET)
	public String indexArticles(ModelMap model) {

		log.debug("start");
				
		model.addAttribute("articleList",articleService.getAllArticlesForChoosing());
		
		log.debug("finish");
		return "articles/browseArticles";
	}
	
	// shows all articles and options to edit, delete, read - for ajax
		@RequestMapping(value="/ajax", method = RequestMethod.GET)
		public String ajaxIndexArticles(ModelMap model) {

			log.debug("start");
					
			model.addAttribute("articleList",articleService.getAllArticlesForChoosing());
			
			log.debug("finish");
			return "articles/partial/browseArticles";
		}
	
	// shows chosen article
	@RequestMapping(value = "/{artykulId}" , method = RequestMethod.GET)
	public String showArticle(@PathVariable String artykulId, ModelMap model) {

		log.debug("start");
		
                int id = Integer.parseInt(artykulId);
                Article a = articleService.getSingleArticle(id);
                model.addAttribute("article", a);
                log.debug("finish");
            
                return "articles/viewArticle";	

		
			
	}
	
		
	
	// views article in edit mode
	@RequestMapping(value = "/{artykulId}/edit", method = RequestMethod.GET)
	public String editArticle(@PathVariable String artykulId, @ModelAttribute(value="article")Article a,
			ModelMap model) {

		log.debug("start");
	
		int id = Integer.parseInt(artykulId);
		Article article = articleService.getSingleArticle(id);
			
		model.addAttribute("action","edit");

		model.addAttribute("article", article);
		model.addAttribute("categoryList",categoryService.getAllCategories());

		log.debug("finish");
		return "articles/addOrUpdateArticle";

	}	

	// posting edited article
	@RequestMapping(value = "/{artykulId}", method = RequestMethod.PUT)
	public String updateArticle(@PathVariable String artykulId,
		@ModelAttribute(value="article")Article a,
			BindingResult result,
			ModelMap model) {
		log.debug("start");
		
		int id = Integer.parseInt(artykulId);
		a.setId(id);
		articleValidator.validate(a, result);
		
		if (result.hasErrors()) {

		    log.debug("errors");
		    
		    model.addAttribute("action","edit");
		    model.addAttribute("categoryList",categoryService.getAllCategories());
		    model.addAttribute("article",a);
			
		    log.debug("finish");
		    return "articles/addOrUpdateArticle";
		}
		
		// in case if you want to edit the same article again
		String articleContent = a.getTresc();
		articleContent = Sanitizer.sanitizeHTML(articleContent);
		a.setTresc(articleContent);
		articleService.update(a);
		model.addAttribute("action","edit");
		model.addAttribute("article",a);
		model.addAttribute("categoryList",categoryService.getAllCategories());
		model.addAttribute("success",true);
		
		log.debug("finish");
		return "articles/addOrUpdateArticle";
		
	}

	// delete article of id X
	@RequestMapping(value = "/delete/{artykulId}", method = RequestMethod.DELETE)
	public @ResponseBody String destroyArticle(@PathVariable String artykulId,@ModelAttribute(value="article")Article a) {
		log.debug("start");
		
		
		int id = Integer.parseInt(artykulId);
		a.setId(id);
		articleService.delete(a);
		log.debug("finish");
		
		return "Pomyślnie usunięto artykuł.";
		
	}
}
