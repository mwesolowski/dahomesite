package pl.daHomeSite.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.daHomeSite.entities.Article;
import pl.daHomeSite.mappers.ArticleMapper;
import pl.daHomeSite.utils.SessionUtility;

@Controller
@RequestMapping("/")
public class Hello {
	private static final Logger log = Logger.getLogger(Hello.class);
	
	@Autowired
	private ArticleMapper articleMapper;
	
	// displays all articles on first page
	@RequestMapping(method = RequestMethod.GET)
	public String welcomeSite(ModelMap model, HttpSession session, HttpServletRequest request) {
	    
		log.debug("start");
		
		List<Article> articlesWithoutContent = articleMapper.getAllWithoutContent();
		model.addAttribute("articles",articlesWithoutContent);
		
		log.debug("finish");
		return "index";
	}
	
}
