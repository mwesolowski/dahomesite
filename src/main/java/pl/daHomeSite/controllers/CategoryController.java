package pl.daHomeSite.controllers;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pl.daHomeSite.entities.Category;
import pl.daHomeSite.services.CategoryService;
import pl.daHomeSite.validators.CategoryValidator;

@Controller
@RequestMapping("/categories")

public class CategoryController {

	private static final Logger log = Logger.getLogger(CategoryController.class);
	
	@Autowired
	private CategoryService categoryService;
	
	// validator for form fields, must be autowired to prevent NullPointerExceptions when querying database inside it
	@Autowired
	private CategoryValidator cv;
		
	//display add category form
	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public String newCategory(ModelMap model) {
		log.debug("start");
		
		model.addAttribute("action","new");
		model.addAttribute("category", new Category());
		
		
		log.debug("finish");
		return "categories/addOrUpdateCategory";
	}
	
	//add new category to database
	@RequestMapping(method = RequestMethod.POST)
	public String createCategory(@ModelAttribute(value="category") Category c, 
			BindingResult result,
			ModelMap model) {
		log.debug("start");
				
		// sets first character upper
		String nazwa = c.getNazwa();
		if(c.getNazwa().trim().length() > 0) {
		    String firstLetter = nazwa.substring(0, 1);
		    String restOfString = nazwa.substring(1);
						
		    nazwa = (firstLetter.toUpperCase() + restOfString).trim(); 
		}
		
		c.setNazwa(nazwa);
		
		// validate
		cv.validate(c, result);
		
		if (result.hasErrors()) {
			log.debug("validation errors");
			model.addAttribute("action","new");
			log.debug("finish");
			return "categories/addOrUpdateCategory";
		}
		//categoryService.findCategoryByName(c.getNazwa());
	
		
		categoryService.add(c);
		model.addAttribute("action","new");
		model.addAttribute("success",true);
		model.addAttribute("category",new Category());
		
		log.debug("finish");
		return "categories/addOrUpdateCategory";
	}
		
	// shows all categories
	@RequestMapping(method = RequestMethod.GET)
	public String indexCategories(ModelMap model) {
		log.debug("start");
		
		List<Category> categoryList = categoryService.getAllCategories();
		
		model.addAttribute("categoryList",categoryList);
		//model.addAttribute("success");
					
		log.debug("finish");
		return "categories/browseCategories";
	}
	// shows all categories - ajax
	@RequestMapping(value="/ajax", method = RequestMethod.GET)
	public String ajaxIndexCategories(ModelMap model) {
			
		log.debug("start");
		
		model.addAttribute("categoryList",categoryService.getAllCategories());
		
		log.debug("finish");
		
		return "categories/partial/browseCategories";
	}
	// shows edit category form
	@RequestMapping(value="/{categoryId}/edit", method = RequestMethod.GET)
	public String editCategory(@PathVariable String categoryId, ModelMap model) {
		log.debug("start");
		
		model.addAttribute("action","edit");
		int id = Integer.parseInt(categoryId);
		Category c = categoryService.getCategoryById(id);
		
		model.addAttribute("category",c);
		
		log.debug("finish");
		return "categories/addOrUpdateCategory";
	}
	
	// process edit form
	@RequestMapping(value="/{categoryId}", method = RequestMethod.PUT)
	public String updateCategory(
			@ModelAttribute(value="category") Category c,
			BindingResult result,
			ModelMap model,
			@PathVariable(value="categoryId") int categoryId
			) {
		log.debug("start");
		
		c.setId(categoryId);
		
		// sets first character upper
		String nazwa = c.getNazwa();
		if(c.getNazwa().trim().length() > 0) {
		    String firstLetter = nazwa.substring(0, 1);
		    String restOfString = nazwa.substring(1);
								
		    nazwa = (firstLetter.toUpperCase() + restOfString).trim();
		}
		
		c.setNazwa(nazwa);
		
		cv.validate(c, result);
		
		if (result.hasErrors()) {
			log.debug("errors");
			model.addAttribute("action","edit");
			log.debug("finish");
			
			return "categories/addOrUpdateCategory";
		}
		
		categoryService.updateCategory(c);
		model.addAttribute("action","edit");
		model.addAttribute("success",true);
		model.addAttribute("category",c);
		
		log.debug("finish");
		return "categories/addOrUpdateCategory";
	}
	
	// process delete category
	@RequestMapping(value="/{categoryId}", method = RequestMethod.DELETE)
	public @ResponseBody String destroyCategory (@ModelAttribute(value="category") Category c,
			ModelMap model, 
			@PathVariable(value="categoryId") String categoryId,
			HttpServletResponse r) {
		log.debug("start");
					
		String response = null;
		int id = Integer.parseInt(categoryId);
		
		c.setId(id);
		
		if (categoryService.countArticlesInCategory(c) > 0) {
		    log.debug("category has articles in it");
		    response = "Przed usunięciem kategorii należy usunąć z niej wszystkie artykuły lub zmienić kategorię tych artykułów.";
		    r.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		    log.debug("finish");
		    return response;
		} else {
		    categoryService.deleteCategory(c); 
		}
	
		response = "Pomyślnie usunięto kategorię.";
		
	
		
		log.debug("finish");
		
		
		return response;
	}
	
}