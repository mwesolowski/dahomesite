package pl.daHomeSite.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pl.daHomeSite.entities.Subpage;
import pl.daHomeSite.services.SubpageService;
import pl.daHomeSite.utils.Sanitizer;
import pl.daHomeSite.validators.SubpageValidator;

@Controller
@RequestMapping("/subpages")

public class SubpageController {

	private static final Logger log = Logger.getLogger(SubpageController.class);
	
	@Autowired
	private SubpageValidator subpageValidator;
	
	@Autowired
	private SubpageService subpageService;
		
	//display add subpage form
	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public String newSubpage(ModelMap model) {
		log.debug("start");
		
		model.addAttribute("action", "new");
		model.addAttribute("subpage", new Subpage());
		
		
		log.debug("finish");
		return "subpages/addOrUpdateSubpage";
	}
	
	// post subpage
	@RequestMapping(method = RequestMethod.POST)
	public String createSubpage(			
			@ModelAttribute(value="subpage") Subpage s,
			BindingResult result,
			ModelMap model) {
		log.debug("start");
		
		
		// for header "Dodaj podstronę"
		model.addAttribute("action", "new");
		
		subpageValidator.validate(s, result);
		

		if (result.hasErrors()) {
			log.debug("errors");
			model.addAttribute("action", "new");
			log.debug("finish");
			return "subpages/addOrUpdateSubpage";
		}
		
		String zawartosc = s.getZawartosc();
		zawartosc = Sanitizer.sanitizeHTML(zawartosc);
		s.setZawartosc(zawartosc);
		subpageService.add(s);
		
		// readies new subpage to add after success
		model.addAttribute("subpage", new Subpage());
		model.addAttribute("success",true);
		
		log.debug("finish");
		return "subpages/addOrUpdateSubpage";
	}
	
	// shows all subpages
	@RequestMapping(method = RequestMethod.GET) 
	public String indexSubpages(ModelMap model) {
		log.debug("start");
				
		model.addAttribute("subpageList",subpageService.getAllSubpages());
		
		log.debug("finish");
		return "subpages/browseSubpages";
	}
	
	// shows all subpages - for Ajax
	@RequestMapping(value="/ajax", method = RequestMethod.GET) 
	public String ajaxIndexSubpages(ModelMap model) {
		log.debug("start");
				
		model.addAttribute("subpageList",subpageService.getAllSubpages());
		
		log.debug("finish");
		return "subpages/partial/browseSubpages";
	}
	
	// displays selected page
	@RequestMapping(value="/{subpageId}", method = RequestMethod.GET)
	public String showSubpage(@PathVariable String subpageId, ModelMap model) {
		log.debug("start");
		
		int id = Integer.parseInt(subpageId);
		Subpage s = subpageService.getSubpage(id);
			
		model.addAttribute("subpage",s);

		log.debug("finish");
		return "subpages/viewSubpage";
	}
	
	
	// shows edit form
	@RequestMapping(value="/{subpageId}/edit", method = RequestMethod.GET)
	public String editSubpage(@PathVariable String subpageId, ModelMap model) {
		log.debug("start");

		int id = Integer.parseInt(subpageId);
		Subpage s = subpageService.getSubpage(id);
		
		model.addAttribute("action","edit");
		model.addAttribute("subpage",s);
		
		log.debug("finish");
		
		return "subpages/addOrUpdateSubpage";
	}
	
	// posts edited subpage
	@RequestMapping(value="/{subpageId}", method = RequestMethod.PUT)
	public String updateSubpage(
			@PathVariable String subpageId,
			@ModelAttribute(value="subpage") Subpage s,
			BindingResult result,
			ModelMap model
			) {
		log.debug("start");
				
		int id = Integer.parseInt(subpageId);
		s.setId(id);
		subpageValidator.validate(s, result);
		
		if (result.hasErrors()) {
			log.debug("errors");
			
			model.addAttribute("action","edit");
			model.addAttribute("subpage",s);
			
			log.debug("finish");

			return "subpages/addOrUpdateSubpage";
			
		}
		String zawartosc = s.getZawartosc();
		zawartosc = Sanitizer.sanitizeHTML(zawartosc);
		s.setZawartosc(zawartosc);
		subpageService.updateSubpage(s);
		model.addAttribute("action","edit");
		model.addAttribute("subpage",s);
		model.addAttribute("success",true);

		log.debug("finish");
		return "subpages/addOrUpdateSubpage";
	}
	
	
	// deletes subpage
	@RequestMapping(value="/delete/{subpageId}", method = RequestMethod.DELETE)
	public @ResponseBody String destroySubpage (@PathVariable String subpageId,
		@ModelAttribute(value="subpage") Subpage s) {
		log.debug("start");

		int id = Integer.parseInt(subpageId);
		s.setId(id);
		subpageService.delete(s);
		
		
		log.debug("finish");
		
		
		return "Pomyślnie usunięto podstronę.";
	}
}