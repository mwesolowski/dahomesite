package pl.daHomeSite.services;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.daHomeSite.controllers.ArticlesController;
import pl.daHomeSite.entities.Subpage;
import pl.daHomeSite.mappers.SubpageMapper;

@Service
public class SubpageService {
	
	private static final Logger log = Logger.getLogger(SubpageService.class);
	
	@Autowired
	private SubpageMapper subpageMapper;

	@Transactional
	public void add(Subpage s) {
		log.debug("start");
		subpageMapper.add(s);
		log.debug("finish");
	}

	@Transactional
	public List<Subpage> getAllSubpages() {
		log.debug("start");
		log.debug("finish");
		// TODO Auto-generated method stub
		return subpageMapper.getAllSubpages();
	}

	@Transactional
	public Subpage getSubpage(int id) {
		// TODO Auto-generated method stub
		log.debug("start");
		log.debug("finish");
		return subpageMapper.getSubpage(id);
	}

	@Transactional
	public void updateSubpage(Subpage s) {
		log.debug("start");
		subpageMapper.updateSubpage(s);
		log.debug("finish");
	}

	@Transactional
	public void delete(Subpage s) {
		log.debug("start");
		subpageMapper.delete(s);
		log.debug("finish");
		
	}



}
