package pl.daHomeSite.services;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.daHomeSite.entities.Article;
import pl.daHomeSite.mappers.ArticleMapper;

@Service
public class ArticleService {
	
	private static final Logger log = Logger.getLogger(ArticleService.class);
	
	@Autowired
	private ArticleMapper articleMapper;

	@Transactional
	public void add(Article a) {
		log.debug("start");
		articleMapper.add(a);
		log.debug("finish");
	}

	@Transactional
	public void update(Article a) {
		log.debug("start");
		articleMapper.update(a);
		log.debug("finish");
	}

	@Transactional
	public void delete(Article a) {
		log.debug("start");
		articleMapper.delete(a);
		log.debug("finish");
	}

	@Transactional
	public List<Article> getAll() {
		log.debug("start");
		log.debug("finish");
		// TODO Auto-generated method stub
		return articleMapper.getAll();
	}

	@Transactional
	public List<Article> getAllArticlesForChoosing() {
		log.debug("start");
		log.debug("finish");
		// TODO Auto-generated method stub
		return articleMapper.getAllArticlesForChoosing();
	}

	@Transactional
	public Article getSingleArticle(int idArtykulu) {
		log.debug("start");
		log.debug("finish");
		// TODO Auto-generated method stub
		return articleMapper.getSingleArticle(idArtykulu);
	}

	@Transactional
	public List<Article> getAllWithoutContent() {
		log.debug("start");
		log.debug("finish");
		// TODO Auto-generated method stub
		return articleMapper.getAllWithoutContent();
	}

}
