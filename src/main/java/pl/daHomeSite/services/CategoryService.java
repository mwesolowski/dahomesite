package pl.daHomeSite.services;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.daHomeSite.controllers.ArticlesController;
import pl.daHomeSite.entities.Category;
import pl.daHomeSite.entities.Subpage;
import pl.daHomeSite.mappers.CategoryMapper;
import pl.daHomeSite.mappers.SubpageMapper;

@Service
public class CategoryService {
	
	private static final Logger log = Logger.getLogger(CategoryService.class);
	
	@Autowired
	private CategoryMapper categoryMapper;

	@Transactional
	public void add(Category c) {
		log.debug("start");
		categoryMapper.add(c);
		log.debug("finish");
	}

	@Transactional
	public Category findCategoryByName(String name) {
		log.debug("start");
		log.debug("finish");
		return categoryMapper.findCategoryByName(name);
	}

	@Transactional
	public List<Category> getAllCategories() {
		// TODO Auto-generated method stub
		log.debug("start");
		log.debug("finish");
		return categoryMapper.getAllCategories();
	}

	@Transactional
	public Category getCategoryById(int categoryId) {
		// TODO Auto-generated method stub
		log.debug("start");
		log.debug("finish");
		return categoryMapper.getCategoryById(categoryId);
	}

	
	@Transactional
	public void updateCategory(Category c) {
		// TODO Auto-generated method stub
		log.debug("start");
		categoryMapper.updateCategory(c);
		log.debug("finish");
		
		
	}

	@Transactional
	public void deleteCategory(Category c) {
		// TODO Auto-generated method stub
		log.debug("start");
		categoryMapper.deleteCategory(c);
		log.debug("finish");
	}
	
	public int countArticlesInCategory(Category c) {
	    log.debug("start");
	    log.debug("finish");
	    return categoryMapper.countArticlesInCategory(c);
	}




}
