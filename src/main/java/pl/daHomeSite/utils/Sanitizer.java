package pl.daHomeSite.utils;

import org.apache.log4j.Logger;
import org.owasp.html.PolicyFactory;
import org.owasp.html.Sanitizers;

public class Sanitizer {
    private static final Logger log = Logger.getLogger(Sanitizer.class);
    
    public static String sanitizeHTML(String unsafeHTML) {
	log.debug("start");
	PolicyFactory pf = Sanitizers.FORMATTING.and(Sanitizers.LINKS);
	
	log.debug("finish");
	return pf.sanitize(unsafeHTML);
    }

}
