package pl.daHomeSite.utils;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import pl.daHomeSite.controllers.ArticlesController;

public class SessionUtility {
	
	private static final Logger log = Logger.getLogger(ArticlesController.class);
	
	private static SessionUtility instance = null;
	
	// removes session attribute of name given and shows current attributes
	public void clearSessionAttribute(HttpServletRequest request, String attrName) {
		log.debug("clearing session attribute of name: " + attrName);
		request.getSession().removeAttribute(attrName);
		showSessionAttributes(request);
		
	}
	
	// shows current session attributes
	public void showSessionAttributes(HttpServletRequest request) {
		log.debug("session attributes:");
		for (Enumeration<String> e = request.getSession().getAttributeNames(); e.hasMoreElements();) {
			String attribName = (String) e.nextElement();
			Object attribValue = request.getSession().getAttribute(attribName);
			
			log.debug(attribName + "=" + attribValue);
		}
	}

	public static SessionUtility getInstance() {
		if (instance == null)
			instance = new SessionUtility();
		
		return instance;
	}
	
	private SessionUtility() {
		
	}

}
