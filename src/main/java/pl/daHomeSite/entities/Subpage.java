package pl.daHomeSite.entities;

import java.io.Serializable;

public class Subpage implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String tytul;
	private String zawartosc;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTytul() {
		return tytul;
	}
	public void setTytul(String tytul) {
		this.tytul = tytul;
	}
	public String getZawartosc() {
		return zawartosc;
	}
	public void setZawartosc(String zawartosc) {
		this.zawartosc = zawartosc;
	}
	
	

}
