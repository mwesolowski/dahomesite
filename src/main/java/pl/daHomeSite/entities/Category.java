package pl.daHomeSite.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Category implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id;
	private String nazwa;
	private List<Article> artykuly = new ArrayList<Article>();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	public List<Article> getArtykuly() {
		return artykuly;
	}
	public void setArtykuly(List<Article> artykuly) {
		this.artykuly = artykuly;
	}
	
	
}
