package pl.daHomeSite.entities;

import java.io.Serializable;
import java.util.Date;

public class Article implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String tytulArtykulu;
	private String wprowadzenie;
	private String tresc;
	private Date dataArtykulu;
	private Category kategoria;
		
	public Article() {
		this.dataArtykulu = new Date();
	}
	public String getTytulArtykulu() {
		return tytulArtykulu;
	}
	public void setTytulArtykulu(String tytulArtykulu) {
		this.tytulArtykulu = tytulArtykulu;
	}
	public String getWprowadzenie() {
		return wprowadzenie;
	}
	public void setWprowadzenie(String wprowadzenie) {
		this.wprowadzenie = wprowadzenie;
	}
	public String getTresc() {
		return tresc;
	}
	public void setTresc(String tresc) {
		this.tresc = tresc;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getDataArtykulu() {
		return dataArtykulu;
	}
	public void setDataArtykulu(Date dataArtykulu) {
		this.dataArtykulu = dataArtykulu;
	}
	public Category getKategoria() {
		return kategoria;
	}
	public void setKategoria(Category kategoria) {
		this.kategoria = kategoria;
	}



}
